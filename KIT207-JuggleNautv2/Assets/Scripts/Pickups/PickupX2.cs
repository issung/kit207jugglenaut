﻿public class PickupX2 : Pickup
{
    public float effectDuration;
    public float timeBetweenShots;

    protected override bool OnPickup(Player player)
    {
        AutoShoot shoot = player.GetComponent<AutoShoot>();

        if (shoot)
        {
            shoot.StartCoroutine(shoot.ModifyShootRate(effectDuration, timeBetweenShots));
            return true;
        }

        return false;
    }
}