﻿using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    public int Health = 1;

    public float UpDownHeight = .5f;
    public float UpDownSpeed = 2f;

    public float RotateSpeed = 1f;

    public float MoveSpeed = 1f;

    private Vector3 m_direction = Vector3.left;
    private float m_yRotation, m_yPosition;

    [SerializeField, ShowOnly]
    private float m_yPositionStart;
    private Player m_player;
    private float m_randomTimeOffset;

    protected virtual void Start()
    {
        m_yPositionStart = transform.GetY() + UpDownHeight / 2f;
        m_randomTimeOffset = Random.Range(0f, 2f);
    }

    protected virtual void Update()
    {
        m_yPosition = m_yPositionStart + (Mathf.Sin((Time.time + m_randomTimeOffset) * UpDownSpeed) * UpDownHeight) / 2f;
        transform.SetY(m_yPosition);

        m_yRotation += RotateSpeed * Controller.GetDtf();
        transform.SetYRotation(m_yRotation);

        transform.position = transform.position + (m_direction * MoveSpeed * Controller.GetDtf());

        if (transform.GetX() < Controller.Instance.GameBounds.min.x)
            Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        m_player = other.gameObject.GetComponent<Player>();

        if (other.gameObject.tag == "Player" && m_player && OnPickup(m_player))
        {
            PickupSpawner.Instance.currentPickups--;
            Destroy(gameObject);
        }
    }

    protected abstract bool OnPickup(Player player);
}