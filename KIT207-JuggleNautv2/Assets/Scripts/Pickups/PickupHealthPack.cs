﻿public class PickupHealthPack : Pickup
{
    protected override bool OnPickup(Player player)
    {
        if (player.CanPickupHealthPack())
        {
            player.AddHealth(Health);
            return true;
        }

        return false;
    }
}