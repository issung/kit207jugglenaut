﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class HealthBar : MonoBehaviour
{
    #pragma warning disable IDE0044, CS0649
    
    private float m_rtMaxWidth;

    [SerializeField] private float m_yGap, m_xGap;

    [SerializeField] private float m_health = 100;
    private float m_healthBuffer;
    [SerializeField] private float m_maxHealth = 100;
    private float m_maxHealthBuffer;

    [Range(0, 1)]
    [Tooltip("Speed for tween to catch up. Higher is Faster. 1 = Instantaneous (No Ease)")]
    public float speed;

    private RectTransform m_outerBarRt, m_innerBarRt;
    private Text m_healthText, m_warningText;

    [SerializeField] private int m_warningThreshold = 20;
    [SerializeField] private float m_warningFlashTime = 1;
    private bool m_warningFlashing = false;

    [SerializeField] private bool m_displayPercentageText;
    string m_textHpFraction;
    string m_textPercentage;

    #pragma warning restore IDE0044, CS0649

    void Awake()
    {
        m_healthBuffer = m_health;
        m_maxHealthBuffer = m_maxHealth;

        m_outerBarRt = GetComponent<RectTransform>();
        m_rtMaxWidth = m_outerBarRt.sizeDelta.x - (m_xGap * 2);
        m_innerBarRt = transform.Find("HealthInnerBar").gameObject.GetComponent<RectTransform>();

        m_innerBarRt.anchoredPosition = new Vector2(m_xGap, m_yGap);
        m_innerBarRt.sizeDelta = new Vector2(m_rtMaxWidth * (m_health / m_maxHealth), m_outerBarRt.sizeDelta.y - (m_yGap * 2));

        m_healthText = transform.Find("HealthText").gameObject.GetComponent<Text>();
        m_warningText = transform.Find("WarningText").gameObject.GetComponent<Text>();
        m_warningText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (m_health < m_healthBuffer)
            m_healthBuffer -= Mathf.Abs(m_health - m_healthBuffer) * speed * Controller.GetDtf();
        else if (m_health > m_healthBuffer)
            m_healthBuffer += Mathf.Abs(m_health - m_healthBuffer) * speed * Controller.GetDtf();

        if (m_maxHealth < m_maxHealthBuffer)
            m_maxHealthBuffer -= Mathf.Abs(m_maxHealth - m_maxHealthBuffer) * speed * Controller.GetDtf();
        else if (m_maxHealth > m_maxHealthBuffer)
            m_maxHealthBuffer += Mathf.Abs(m_maxHealth - m_maxHealthBuffer) * speed * Controller.GetDtf();

        if (m_health <= m_warningThreshold)
            if (!m_warningFlashing)
                StartCoroutine(WarningFlash());

        m_rtMaxWidth = m_outerBarRt.sizeDelta.x - (m_xGap * 2);
        float width = m_rtMaxWidth * (m_healthBuffer / m_maxHealthBuffer);
        m_innerBarRt.sizeDelta = new Vector2(Mathf.Clamp(width, 0, m_rtMaxWidth), m_outerBarRt.sizeDelta.y - (m_yGap * 2));
        m_innerBarRt.anchoredPosition = new Vector2(m_xGap, m_yGap);

        m_textHpFraction = Mathf.RoundToInt(Mathf.Clamp(m_healthBuffer, 0, m_maxHealthBuffer)) + "/" + Mathf.RoundToInt(m_maxHealthBuffer);
        m_textPercentage = m_displayPercentageText ? (" - " + Mathf.RoundToInt(((m_healthBuffer / m_maxHealthBuffer) * 100)) + "%") : "";
        m_healthText.text = m_textHpFraction + m_textPercentage;
    }

    IEnumerator WarningFlash()
    {
        if (m_warningFlashing)        //exit if already flashing.
            yield return null;

        m_warningFlashing = true;
        while (m_health <= m_warningThreshold || m_warningText.gameObject.activeSelf)
        {
            m_warningText.gameObject.SetActive(true);
            yield return new WaitForSeconds(m_warningFlashTime);
            m_warningText.gameObject.SetActive(false);
            yield return new WaitForSeconds(m_warningFlashTime);
        }
        m_warningText.gameObject.SetActive(false);
        m_warningFlashing = false;
        yield return null;
    }

    public void SetHealth(float health) => m_health = health;

    public void SetMaxHealth(float maxHealth) => m_maxHealth = maxHealth > 0 ? maxHealth : m_maxHealth;

    public float GetHealth() => m_health;
}