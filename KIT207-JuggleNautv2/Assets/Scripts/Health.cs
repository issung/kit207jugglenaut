﻿using System;
using UnityEngine;

public class Health : MonoBehaviour
{
    [ShowOnly, SerializeField]
    private float m_health;
    public float maxHealth = 100f;
    public bool destroyWhenDead = true;

    public bool HasHealth => m_health > 0f;
    public bool IsDead => m_health <= 0f;

    public float Fraction => m_health / maxHealth;

    public event EventHandler Death;

    private void Start() => m_health = maxHealth;

    private void FixedUpdate()
    {
        if (IsDead)
        {
            OnDeath();
            gameObject.transform.root.gameObject.SetActive(false);
        }

        //if (destroyWhenDead && IsDead)
        //    Destroy(transform.root.gameObject);
    }

    protected void OnDeath() => Death?.Invoke(this, EventArgs.Empty);

    public void AddHealth(float amount) => m_health = Mathf.Min(m_health + amount, maxHealth);
    public void RemoveHealth(float amount) => m_health = Mathf.Max(m_health - amount, 0);
}