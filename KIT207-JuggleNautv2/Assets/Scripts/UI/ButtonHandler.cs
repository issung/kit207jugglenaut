﻿using UnityEngine;
using UnityEngine.UI;

public abstract class ButtonHandler : MonoBehaviour
{
    protected Button m_button;

    //protected virtual AudioClip PressSound => AudioManager.Instance.ui_confirm;

    private void Start()
    {
        m_button = GetComponent<Button>();

        m_button.onClick.AddListener(() =>
        {
            //if (PressSound)
            //    AudioManager.Instance.player.PlayAndWait(PressSound);

            OnClick();
        });
    }

    private void OnDestroy() => m_button.onClick.RemoveListener(OnClick);

    public abstract void OnClick();
}