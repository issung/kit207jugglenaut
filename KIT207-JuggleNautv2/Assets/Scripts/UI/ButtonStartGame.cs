﻿using UnityEngine.SceneManagement;

public class ButtonStartGame : ButtonHandler
{
    public override void OnClick() => SceneManager.LoadScene("GameScene");
}