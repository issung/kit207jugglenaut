﻿using UnityEngine;

public class ShipDisplayMenu : MonoBehaviour
{
    public float yRotation;
    
    private void Update()
    {
        transform.Rotate(0f, yRotation * Time.deltaTime, 0f, Space.Self);
    }
}