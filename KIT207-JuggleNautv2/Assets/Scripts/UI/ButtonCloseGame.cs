﻿using UnityEngine;

public class ButtonCloseGame : ButtonHandler
{
    public override void OnClick() => Application.Quit();
}