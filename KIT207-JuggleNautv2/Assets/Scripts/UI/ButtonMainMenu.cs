﻿using UnityEngine.SceneManagement;

public class ButtonMainMenu : ButtonHandler
{
    public override void OnClick() => SceneManager.LoadScene("MainMenu");
}