﻿using UnityEngine;

public class RotateSky : MonoBehaviour
{
    private float rotation;

    [Tooltip("Larger is slower. 100 is a nice slow speed.")]
    public int speed = 100;

    
    private void FixedUpdate()
    {
        rotation += Controller.GetDtf() / speed;
        RenderSettings.skybox.SetFloat("_Rotation", rotation);
    }
}