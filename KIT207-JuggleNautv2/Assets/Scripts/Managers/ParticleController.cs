﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public enum Effect { Explosion, HealthPuff };

    public static ParticleController instance;

    private Dictionary<Effect, List<ParticleSystem>> pool;

    // Start is called before the first frame update
    void Awake()
    {
        ParticleController.instance = this;
    }

    public void DoParticle(Effect effect, Vector3 position)
    {
        switch (effect)
        {
            case Effect.Explosion:
                break;
            case Effect.HealthPuff:
                break;
        }
    }
}
