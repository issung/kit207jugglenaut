﻿using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{
    public AudioClip BackgroundMusicClip;

    private AudioSource m_audioSource;

    private void Awake()
    {
        m_audioSource = gameObject.AddComponent<AudioSource>();
        //m_audioSource.priority = 256;
        m_audioSource.playOnAwake = true;
        m_audioSource.clip = BackgroundMusicClip;
    }
}