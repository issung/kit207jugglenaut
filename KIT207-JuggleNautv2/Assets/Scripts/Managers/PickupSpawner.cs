﻿using System.Collections;
using UnityEngine;

public class PickupSpawner : UnitySingleton<PickupSpawner>
{
    public override bool Persistent => false;
    
    #pragma warning disable IDE0044, CS0649

    [SerializeField] private bool m_debug;

    public Bounds spawnBounds;
    public float minSpawnTime;
    public float maxSpawnTime;
    public float startDelay;
    public int maxPickups;

    [ShowOnly]
    public int currentPickups;

    private WaitForSeconds m_maxSpawnWait;
    private Coroutine m_spawnCoroutine;
    private BasicRandomList<GameObject> m_pickups;

    #pragma warning restore IDE0044, CS0649

    public override void Awake()
    {
        base.Awake();

        m_pickups = new BasicRandomList<GameObject>
        {
            PrefabLinks.Instance.pickupHealth,
            PrefabLinks.Instance.pickupFireRate,
            //PrefabLinks.Instance.pickupFirePower
        };

        m_maxSpawnWait = new WaitForSeconds(maxSpawnTime);
    }

    private void Start()
    {
        m_spawnCoroutine = StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(startDelay);

        while (true)
        {
            if (currentPickups < maxPickups)
            {
                Instantiate(
                    m_pickups.Random,
                    new Vector3(
                        spawnBounds.center.x + Random.Range(-spawnBounds.extents.x, spawnBounds.extents.x),
                        spawnBounds.center.y + Random.Range(-spawnBounds.extents.y, spawnBounds.extents.y)),
                    Quaternion.identity);

                currentPickups++;
            }
            else
                yield return m_maxSpawnWait;

            yield return new WaitForSeconds(Random.Range(minSpawnTime, maxSpawnTime));
        }
    }

    private void OnDrawGizmos()
    {
        if (m_debug)
        {
            Gizmos.color = new Color(0f, 1f, 1f, 0.4f);
            Gizmos.DrawCube(spawnBounds.center, spawnBounds.size);
        }
    }
}