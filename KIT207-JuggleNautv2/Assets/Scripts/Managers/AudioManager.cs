﻿using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioPlayer))]
public class AudioManager : UnitySingleton<AudioManager>
{
    [ShowOnly] public AudioPlayer player;
    [ShowOnly] public AudioPlayer shootPlayer;
    
    [ShowOnly] public AudioClip ui_confirm;
    [ShowOnly] public AudioClip ui_cancel;

    [ShowOnly] public AudioClip playerHit1;
    [ShowOnly] public AudioClip playerHit2;
    [ShowOnly] public AudioClip playerHit3;
    [ShowOnly] public AudioClip playerHit4;
    [ShowOnly] public AudioClip playerHit5;
    [ShowOnly] public AudioClip playerHit6;
    [ShowOnly] public AudioClip playerHit7;
    [ShowOnly] public AudioClip playerHit8;
    [ShowOnly] public AudioClip playerHit9;

    [ShowOnly] public Dictionary<string, AudioClip> audioClips;

    [ShowOnly] public BasicRandomList<AudioClip> playerHitClips;

    public override void Awake()
    {
        base.Awake();

        player = gameObject.AddComponent<AudioPlayer>();
        player.audioSource.playOnAwake = false;

        shootPlayer = gameObject.AddComponent<AudioPlayer>();
        shootPlayer.audioSource.volume = 0.125f;

        audioClips = new Dictionary<string, AudioClip>();
        var clips = Resources.LoadAll<AudioClip>(@"Audio\");

        for (int i = 0; i < clips.Length; i++)
            audioClips.Add(clips[i].name, clips[i]);

        //ui_confirm = audioClips["ui_confirm"];
        //ui_cancel = audioClips["ui_cancel"];

        playerHitClips = new BasicRandomList<AudioClip>
        {
            (playerHit1 = audioClips["hit_1"]),
            (playerHit2 = audioClips["hit_2"]),
            (playerHit3 = audioClips["hit_3"]),
            (playerHit4 = audioClips["hit_4"]),
            (playerHit5 = audioClips["hit_5"]),
            (playerHit6 = audioClips["hit_6"]),
            (playerHit7 = audioClips["hit_7"]),
            (playerHit8 = audioClips["hit_8"]),
            (playerHit9 = audioClips["hit_9"])
        };
    }
}