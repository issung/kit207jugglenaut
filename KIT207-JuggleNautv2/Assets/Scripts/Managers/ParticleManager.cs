﻿using UnityEngine;
using static UnityEngine.ParticleSystem;

public class ParticleManager : UnitySingleton<ParticleManager>
{
    [ShowOnly, SerializeField] private ParticleSystem m_sparks;
    [ShowOnly, SerializeField] private ParticleSystem m_smoke;

    [ShowOnly, SerializeField] private GameObject m_sparksInst;
    [ShowOnly, SerializeField] private GameObject m_smokeInst;

    [ShowOnly, SerializeField] private MainModule m_mainModule;

    public override void Awake()
    {
        base.Awake();

        m_sparksInst = Instantiate(PrefabLinks.Instance.sparkParticles);
        m_smokeInst = Instantiate(PrefabLinks.Instance.smokeParticles);

        m_sparks = m_sparksInst.GetComponent<ParticleSystem>();
        m_smoke = m_smokeInst.GetComponent<ParticleSystem>();

        DontDestroyOnLoad(m_sparksInst);
        DontDestroyOnLoad(m_smokeInst);
    }

    public void Spawn(ParticleSystem system, Vector3 position)
    {
        m_mainModule = system.main;
        system.transform.position = position;
        system.Play();
    }

    public void SpawnSparks(Vector3 position) => Spawn(m_sparks, position);
    public void SpawnSmoke(Vector3 position) => Spawn(m_smoke, position);
}