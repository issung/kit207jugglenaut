﻿using TMPro;
using UnityEngine;

public class HUD : UnitySingleton<HUD>
{
    public override bool Persistent => false;
    
    #pragma warning disable IDE0044, CS0649

    [SerializeField] private GameObject m_infoBackground;
    [SerializeField] private TextMeshProUGUI m_waveText;
    [SerializeField] private TextMeshProUGUI m_scoreText;

    [SerializeField] private GameObject m_gameEndScreen;
    [SerializeField] private TextMeshProUGUI m_gameEndText;

    #pragma warning restore IDE0044, CS0649

    public bool HasGameEnded { get; private set; }

    public override void Awake()
    {
        base.Awake();
        m_gameEndScreen.SetActive(false);
    }

    public void UpdateWaveText(int wave) => m_waveText.text = $"Wave: {wave + 1}";

    public void UpdateScore(float score) => m_scoreText.text = $"Score: {score:N0}";

    public void SetGameEnded(bool won)
    {
        HasGameEnded = true;
        m_infoBackground.SetActive(false);
        m_gameEndScreen.SetActive(true);
        m_gameEndText.text = $"You {(won ? "won" : "lost")}!\nScore: {Player.Score:N1}";
    }
}