﻿using System.Collections.Generic;

/// <summary>
/// This class is work in progress and is likely to change in the future.
/// </summary>
public struct WaveInfo
{
    //public float StartDeley { get; set; }
    //public int Round { get; set; }
    public EnemySpawnInfo[] EnemySpawns { get; set; }
}