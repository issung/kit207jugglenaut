﻿public enum EnemyType
{
    Generic,
    Dodger,
    Fast,
    Tough
}