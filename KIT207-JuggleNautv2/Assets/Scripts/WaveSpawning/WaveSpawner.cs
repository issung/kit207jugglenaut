﻿using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : UnitySingleton<WaveSpawner>
{
    public override bool Persistent => false;

    public static WaveInfo[] Waves { get; }

    static WaveSpawner()
    {
        Waves = new WaveInfo[]
        {
            // Wave #1
            new WaveInfo
            {
                EnemySpawns = new EnemySpawnInfo[]
                {
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 8,
                        Type = EnemyType.Generic
                    }
                }
            },

            // Wave #2
            new WaveInfo
            {
                EnemySpawns = new EnemySpawnInfo[]
                {
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 10,
                        Type = EnemyType.Generic
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 4,
                        Type = EnemyType.Fast
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 5,
                        Type = EnemyType.Generic
                    },
                }
            },

            // Wave #3
            new WaveInfo
            {
                EnemySpawns = new EnemySpawnInfo[]
                {
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 4,
                        Type = EnemyType.Generic
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 8,
                        Type = EnemyType.Fast
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 3,
                        Type = EnemyType.Generic
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 1,
                        Type = EnemyType.Tough
                    }
                }
            },

            // Wave #4
            new WaveInfo
            {
                EnemySpawns = new EnemySpawnInfo[]
                {
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 12,
                        Type = EnemyType.Fast
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 8,
                        Type = EnemyType.Generic
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 4,
                        Type = EnemyType.Tough
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 5,
                        Type = EnemyType.Dodger
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 8,
                        Type = EnemyType.Fast
                    }
                },
            },

            // Wave #5
            new WaveInfo
            {
                EnemySpawns = new EnemySpawnInfo[]
                {
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 20,
                        Type = EnemyType.Fast
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 4,
                        Type = EnemyType.Generic
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 14,
                        Type = EnemyType.Tough
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 8,
                        Type = EnemyType.Dodger
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 6,
                        Type = EnemyType.Fast
                    },
                    new EnemySpawnInfo
                    {
                        SpawnAmount = 6,
                        Type = EnemyType.Tough
                    }
                }
            }
        };
    }
    
    #pragma warning disable IDE0044, CS0649

    [SerializeField] private bool m_debug;

    public Bounds SpawnBounds;
    public float WaveStartDelay = 1f;
    public float SpawnDelayMin = 1f;
    public float SpawnDelayMax = 3f;
    public int currentWave = 0;
    public int currentEnemyIndex = 0;

    [ShowOnly]
    public int enemiesSpawned;

    [ShowOnly, SerializeField] private List<GameObject> m_spawned;
    [ShowOnly, SerializeField] private bool m_waveStarted;

    //private float m_startTime;
    [ShowOnly, SerializeField] private float m_currenWaveStartTime;
    [ShowOnly, SerializeField] private float m_currenSpawnTime;

    [ShowOnly, SerializeField] private float m_nextSpawn;
    [ShowOnly, SerializeField] private bool m_lastWave;
    [ShowOnly, SerializeField] private bool m_allWavesCompleted;

    #pragma warning restore IDE0044, CS0649

    private bool m_gameEndShown;

    public override void Awake()
    {
        base.Awake();

        m_spawned = new List<GameObject>(20);

        #region Test Logic

        /*List<GameObject> waveEnemies = new List<GameObject>();

        EnemySpawnInfo info = new EnemySpawnInfo(3, EnemyType.Generic);

        for (int i = 0; i < Waves.Length; i++)
        {
            for (int j = 0; j < Waves[i].EnemySpawns.Length; j++)
            {
                waveEnemies.Add(PrefabLinks.Instance.GetEnemy(info.Type));
            }
        }*/

        #endregion

        m_waveStarted = false;
    }

    private void Start()
    {
        //m_startTime = Time.time;
    }

    private void FixedUpdate()
    {
        if (!m_waveStarted)
        {
            m_currenWaveStartTime += Time.fixedDeltaTime;

            if (m_currenWaveStartTime >= WaveStartDelay)
                m_waveStarted = true;
        }
        else
            m_currenWaveStartTime = 0f;

        if (m_waveStarted && !m_lastWave)
        {
            m_currenSpawnTime += Time.fixedDeltaTime;

            if (m_currenSpawnTime >= m_nextSpawn)
            {
                m_currenSpawnTime -= m_nextSpawn;
                m_nextSpawn = Random.Range(SpawnDelayMin, SpawnDelayMax);

                m_spawned.Add(Instantiate(
                    PrefabLinks.Instance.GetEnemy(Waves[currentWave].EnemySpawns[currentEnemyIndex].Type),
                    new Vector3(
                        SpawnBounds.min.x,
                        Random.Range(SpawnBounds.min.y, SpawnBounds.max.y)),
                    Quaternion.identity));

                // This current implementation only spawns all of each enemy.
                if (--Waves[currentWave].EnemySpawns[currentEnemyIndex].SpawnAmount <= 0)
                {
                    if (++currentEnemyIndex >= Waves[currentWave].EnemySpawns.Length)
                    {
                        currentEnemyIndex = 0;
                        currentWave++;

                        if (currentWave > Waves.Length - 1)
                        {
                            currentWave = 0;
                            m_lastWave = true;
                        }
                        else
                            HUD.Instance.UpdateWaveText(currentWave);
                    }
                }
            }
        }

        if (!m_gameEndShown && m_lastWave && enemiesSpawned == 0)
        {
            m_allWavesCompleted = true;
            HUD.Instance.SetGameEnded(true);
            m_gameEndShown = true;
        }
    }

    private void OnDrawGizmos()
    {
        if (m_debug)
        {
            Gizmos.color = new Color(0f, 1f, 1f, 0.4f);
            Gizmos.DrawCube(SpawnBounds.center, SpawnBounds.size);
        }
    }

    private void OnGUI()
    {
        if (m_debug)
        {
            string text =
                $"Current Enemy Index: {currentEnemyIndex}\n" +
                $"Next enemy in: {m_nextSpawn - m_currenSpawnTime:N1}s\n" +
                $"Enemies Spawned: {enemiesSpawned}";

            var textSize = GUI.skin.box.CalcSize(new GUIContent(text));

            MyDebug.GuiAutoBox(
                Screen.width - textSize.x - 16f,
                Screen.height - textSize.y - 16f, text);
        }
    }
}