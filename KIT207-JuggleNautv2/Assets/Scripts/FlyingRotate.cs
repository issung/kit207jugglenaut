﻿using UnityEngine;

public class FlyingRotate : MonoBehaviour
{
    public bool upAndDown, backAndForth;
    public int multiplier;
    public int limit = 15;
    public float ease;
    public Vector3 startRotation;

    private Vector3 m_lastFramePosition = Vector3.zero;

    private void Start()
    {
        startRotation = gameObject.transform.eulerAngles;
    }

    void Update()
    {
        Controller.ClampTransformToGameBounds(gameObject.transform);

        Vector3 positionDifference = m_lastFramePosition - transform.position;

        float xrot = 0;
        float zrot = 0;

        // We rotate the x axis depending on the y position difference.
        if (upAndDown)
        {
            float xgoal = Mathf.Clamp(positionDifference.y * multiplier, -limit, limit);
            xrot = transform.rotation.x + ((xgoal - transform.rotation.x) * ease);
            //xrot = Mathf.Lerp(transform.rotation.x, xgoal, ease);
        }

        // We rotate on the z axis depending on the x position difference.
        if (backAndForth)
        {
            float zgoal = Mathf.Clamp(positionDifference.x * multiplier, -limit, limit);
            zrot = transform.rotation.z + ((zgoal - transform.rotation.z) * ease);
            //zrot = Mathf.Lerp(transform.rotation.z, zgoal, ease);
        }

        transform.rotation = Quaternion.Euler(startRotation.x + xrot, startRotation.y, startRotation.z + zrot);

        m_lastFramePosition = transform.position;
    }
}