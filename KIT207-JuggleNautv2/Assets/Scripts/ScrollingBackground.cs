﻿using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    #pragma warning disable IDE0044, CS0649

    [SerializeField] private bool m_center;

    #pragma warning restore IDE0044, CS0649

    void Awake()
    {
        if (m_center)
        {
            LeanTween.value(gameObject, (float f) =>
            {
                transform.position = new Vector3(f, transform.position.y, transform.position.z);
            }, transform.position.x, -56, 15).setOnComplete(() => { MoveFromStartToEnd(); });
        }
        else
            MoveFromStartToEnd();
    }

    private void MoveFromStartToEnd()
    {
        LeanTween.value(gameObject, (float f) =>
        {
            transform.position = new Vector3(f, transform.position.y, transform.position.z);
        }, 56, -56, 30).setOnComplete(() => { MoveFromStartToEnd(); });
    }
}