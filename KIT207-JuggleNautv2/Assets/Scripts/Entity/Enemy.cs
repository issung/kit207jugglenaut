﻿using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    public static bool IsAnyShipInView { get; private set; }

    protected abstract EnemyType EnemyType { get; }
    
    #pragma warning disable IDE0044, CS0649

    //[SerializeField] private int m_health;
    [SerializeField] protected float MoveSpeed;
    [SerializeField] protected float ScoreGain;

    #pragma warning restore IDE0044, CS0649

    protected AutoShoot autoShoot;
    protected new Rigidbody rigidbody;
    protected Health health;

    protected virtual void Awake()
    {
        autoShoot = GetComponent<AutoShoot>();
        autoShoot.shouldShoot = false;

        rigidbody = GetComponent<Rigidbody>();
        health = GetComponentInChildren<Health>();
        health.Death += OnDeath;

        WaveSpawner.Instance.enemiesSpawned++;
    }

    /*private void OnBecameVisible() => IsAnyShipInView = true;

    private void OnBecameInvisible()
    {
        if (WaveSpawner.Instance.enemiesSpawned == 0)
            IsAnyShipInView = false;
    }*/

    protected virtual void Update()
    {
        if (!HUD.Instance.HasGameEnded)
            gameObject.transform.position += Vector3.left * MoveSpeed * Time.deltaTime;

        if (transform.GetX() < 14)
        {
            autoShoot.shouldShoot = true;
            IsAnyShipInView = true;
        }

        if (transform.GetX() < Controller.Instance.GameBounds.min.x)
        {
            if (--WaveSpawner.Instance.enemiesSpawned <= 0)
                IsAnyShipInView = false;

            Destroy(gameObject);
        }

        Controller.ClampTransformToGameBounds(gameObject.transform);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerBullet")
        {
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            health.RemoveHealth(bullet.damage);
            bullet.HitSomething();

            rigidbody.AddForce(bullet.lastFrameDirection * bullet.knockback);
        }
    }

    private void OnDeath(object sender, System.EventArgs e)
    {
        WaveSpawner.Instance.enemiesSpawned--;
        Player.Score += ScoreGain;
    }

    //private void OnGUI() => MyDebug.GuiBoxObj(transform, $"{EnemyType}");
}
