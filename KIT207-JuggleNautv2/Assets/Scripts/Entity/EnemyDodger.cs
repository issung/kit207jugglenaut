﻿using UnityEngine;

public class EnemyDodger : Enemy
{
    protected override EnemyType EnemyType => EnemyType.Dodger;

    [SerializeField] private float m_moveHeight = 1f;
    [SerializeField] private float m_speed = 1f;
    [SerializeField] private float m_minSpeed;
    [SerializeField] private float m_maxSpeed;
    [SerializeField] private float m_yPositionStart;

    private float m_yRotation, m_yPosition;
    //private float m_randomTimeOffset;
    
    private void Start()
    {
        m_yPositionStart = transform.GetY() + m_moveHeight / 2f;
        //m_randomTimeOffset = Random.Range(0f, 2f);
    }

    private void FixedUpdate()
    {
        //m_yPosition = m_yPositionStart + (Mathf.Sin(Time.time * Random.Range(m_minSpeed, m_maxSpeed)) /** Random.Range(0f, 1f)*/ * m_moveHeight / 2f);
        m_yPosition = m_yPositionStart + (Mathf.Sin(Time.time * m_speed) /** Random.Range(0f, 1f)*/ * m_moveHeight / 2f);
        transform.SetY(m_yPosition);
    }
}