﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGeneric : Enemy
{
    protected override EnemyType EnemyType => EnemyType.Generic;
}