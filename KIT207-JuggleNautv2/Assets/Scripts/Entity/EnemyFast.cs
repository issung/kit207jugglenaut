﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFast : Enemy
{
    protected override EnemyType EnemyType => EnemyType.Fast;
}