﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Credit goes to "Jayanam Games" for this script.
   Found here: https://www.patreon.com/posts/unity-3d-drag-22917454. */

public class Player : MonoBehaviour
{
    private static float m_score;

    public static float Score
    {
        get => m_score;
        set
        {
            if (m_score != value)
            {
                m_score = value;
                HUD.Instance.UpdateScore(value);
            }
        }
    }

    private Vector3 m_offset;
    private float m_zCoord;
    private Rigidbody m_rigidbody;

    #pragma warning disable IDE0044, CS0649

    //Defaults are x=11.2 & y=6
    [SerializeField] private float m_xRange;
    [SerializeField] private float m_yMin, m_yMax;
    [SerializeField] private HealthBar m_healthbar;
    [SerializeField] private float m_health, m_maxHealth;

    private AutoShoot m_autoShoot;

    #pragma warning restore IDE0044, CS0649

    private bool m_dragging = false;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_autoShoot = GetComponent<AutoShoot>();
    }

    void OnMouseDown()
    {
        m_zCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;

        //m_rigidbody.position = transform.position;

        // Store offset = gameobject world pos - mouse world pos
        m_offset = gameObject.transform.position - GetMouseAsWorldPoint();

        m_dragging = true;
    }

    internal void AddHealth(float add)
    {
        m_health += add;
        ClampHealth();
        m_healthbar.SetHealth(m_health);
    }

    public bool CanPickupHealthPack() => m_health < m_maxHealth;

    private void ClampHealth()
    {
        m_health = Mathf.Clamp(m_health, 0, m_maxHealth);
    }

    private void OnMouseUp()
    {
        m_dragging = false;
    }

    private void Update()
    {
        if (m_dragging && !HUD.Instance.HasGameEnded)
        {
            /*m_rigidbody.position = GetMouseAsWorldPoint() + m_offset;

            m_rigidbody.position = new Vector3(
                Mathf.Clamp(m_rigidbody.position.x, -m_xRange, m_xRange),
                Mathf.Clamp(m_rigidbody.position.y, -m_yRange, m_yRange),
                m_rigidbody.position.z);*/

            transform.position = GetMouseAsWorldPoint() + m_offset;

            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, -m_xRange, m_xRange),
                Mathf.Clamp(transform.position.y, m_yMin, m_yMax),
                transform.position.z);
        }

        m_autoShoot.shouldShoot = WaveSpawner.Instance.enemiesSpawned > 0;
    }

    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;

        // z coordinate of game object on screen
        mousePoint.z = m_zCoord;

        // Convert it to world points
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    /*void OnMouseDrag()
    {
        //rb.position = GetMouseAsWorldPoint() + mOffset;

        //rb.position = new Vector3(
        //    Mathf.Clamp(rb.position.x, -xRange, xRange),
        //    Mathf.Clamp(rb.position.y, -yRange, yRange),
        //    rb.position.z);

        transform.position = GetMouseAsWorldPoint() + mOffset;

        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, -xRange, xRange),
            Mathf.Clamp(transform.position.y, -yRange, yRange),
            transform.position.z);
    }*/

    private void Hit(float damage)
    {
        // Subtract health
        m_health -= damage;

        // Tell health bar new health value.
        m_healthbar.SetHealth(m_health);

        // Camera effects.
        LeanTween.value(gameObject, (float i) =>
        {
            CameraEffectsController.SetEffect(CameraEffectsController.Effect.ColorDrift, i);
            CameraEffectsController.SetEffect(CameraEffectsController.Effect.ScanLineJitter, i);
        }, 0.05f, 0f, 1f);

        LeanTween.value(gameObject, (float i) =>
        {
            CameraEffectsController.SetEffect(CameraEffectsController.Effect.DigitalGlitch, i);
        }, 0.025f, 0f, 0.5f);

        AudioManager.Instance.player.PlayAndWait(AudioManager.Instance.playerHitClips.Random);

        if (m_health <= 0)
            HUD.Instance.SetGameEnded(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet")
        {
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            Hit(bullet.damage);
            bullet.HitSomething();

            m_rigidbody.AddForce(bullet.lastFrameDirection * bullet.knockback);
        }
    }
}
