﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTough : Enemy
{
    protected override EnemyType EnemyType => EnemyType.Tough;
}