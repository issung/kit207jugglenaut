﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Thruster : MonoBehaviour
{
    MeshFilter mf;
    Mesh mesh;

    // Start is called before the first frame update
    void Awake()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    // Update is called once per frame
    void Update()
    {
        var newverts = new Vector3[mesh.vertexCount];

        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            newverts[i] = mesh.vertices[i];
            newverts[i].y = Mathf.Clamp(newverts[i].y + Random.Range(-0.1f, 0.1f), -1, 0);
        }

        mesh.SetVertices(newverts.ToList());
    }
}
