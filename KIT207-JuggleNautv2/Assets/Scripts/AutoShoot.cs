﻿using System.Collections;
using UnityEngine;

public class AutoShoot : MonoBehaviour
{
    public Transform bulletSpawn;

    public float damage;
    public float knockback;

    public bool friendly;
    public Vector3 direction;
    public float bulletSpeed;

    public float timeBetweenShots;
    public float shootingStartDelay;
    public int burstAmount = 0;
    public float burstDelay;

    public AudioClip shootSound;

    [ShowOnly]
    public bool shouldShoot = true;

    #pragma warning disable IDE0044, CS0649
    private bool m_shooting = true;

    [SerializeField] private Material m_bulletMaterial;

    #pragma warning restore IDE0044, CS0649

    private int m_bulletsFired;

    private void Awake()
    {
        m_bulletsFired = 0;
        this.InvokeDelay(shootingStartDelay, () => StartCoroutine(ShootingCoroutine()));
    }

    private IEnumerator ShootingCoroutine()
    {
        while (m_shooting)
        {
            if (HUD.Instance.HasGameEnded)
            {
                m_shooting = false;
                yield break;
            }

            if (shouldShoot && Enemy.IsAnyShipInView)
            {
                DirectionBullet bullet = Controller.GetBullet(Bullet.Type.Direction) as DirectionBullet;
                bullet.damage = damage;
                bullet.Shoot(bulletSpawn.position, m_bulletMaterial, friendly, direction, bulletSpeed);
                AudioManager.Instance.shootPlayer.PlayAndWait(shootSound);

                if (burstAmount > 0 && ++m_bulletsFired >= burstAmount)
                {
                    m_bulletsFired -= burstAmount;
                    yield return new WaitForSeconds(burstDelay);
                }
            }

            yield return new WaitForSeconds(timeBetweenShots);
        }
    }

    public IEnumerator ModifyShootRate(float duration, float timeBetweenShots)
    {
        float prevTimeBetweenShots = this.timeBetweenShots;
        this.timeBetweenShots = timeBetweenShots;

        yield return new WaitForSeconds(duration);

        this.timeBetweenShots = prevTimeBetweenShots;
    }
}
