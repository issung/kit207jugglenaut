﻿using UnityEngine;

public class HealthDisplay : MonoBehaviour
{
    [ShowOnly]
    public Health health;
    
    public float width, height;

    public GameObject background;
    public GameObject foreground;

    public Color startColour, endColour;

    private SpriteRenderer m_fgSpriteRenderer;

    private void Awake()
    {
        health = GetComponentInParent<Health>();
        transform.localScale = new Vector3(width, height, 1f);
        m_fgSpriteRenderer = foreground.GetComponent<SpriteRenderer>();
    }
    
    private void Update()
    {
        if (health)
        {
            float fgWidth = Mathf.Clamp(health.Fraction, 0, health.maxHealth);
            foreground.transform.position = foreground.transform.position.SetX(background.transform.position.x - (width / 2) + (fgWidth / 2));
            foreground.transform.localScale = foreground.transform.localScale.SetX(fgWidth);
            //m_fgSpriteRenderer.color = Color.HSVToRGB(health.Fraction * 120.0f / 360.0f, 1, 1);
            m_fgSpriteRenderer.color = Color.Lerp(startColour, endColour, 1f - health.Fraction);
        }
    }
}