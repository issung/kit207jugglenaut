﻿using UnityEngine;

public class PrefabLinks : UnitySingleton<PrefabLinks>
{
    #region Entities

    public GameObject bulletPrefab;
    public GameObject playerShip;

    public GameObject enemyGeneric;
    public GameObject enemyDodger;
    public GameObject enemyFast;
    public GameObject enemyTough;

    #endregion

    #region Pickups

    public GameObject pickupHealth;
    public GameObject pickupFireRate;
    public GameObject pickupFirePower;

    #endregion

    #region UI

    public GameObject healthBar;

    #endregion

    #region Particles

    public GameObject sparkParticles;
    public GameObject smokeParticles;

    #endregion

    public GameObject GetEnemy(EnemyType type) =>
        type == EnemyType.Generic ? enemyGeneric :
        type == EnemyType.Dodger ? enemyDodger :
        type == EnemyType.Fast ? enemyFast :
        type == EnemyType.Tough ? enemyTough : throw new System.Exception();
}